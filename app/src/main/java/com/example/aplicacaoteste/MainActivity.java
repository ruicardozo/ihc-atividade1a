package com.example.aplicacaoteste;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

public class MainActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText editTextParcela1 = findViewById(R.id.editTextParcela1);
        EditText editTextParcela2 = findViewById(R.id.editTextParcela2);
        TextView textViewResultado = findViewById(R.id.textViewResultado);
        Button buttonSoma = findViewById(R.id.buttonSoma);

        buttonSoma.setOnClickListener(view -> {
            double parcela1 = Double.parseDouble(editTextParcela1.getText().toString());
            double parcela2 = Double.parseDouble(editTextParcela2.getText().toString());
            double soma = parcela1 + parcela2;
            textViewResultado.setText(String.format(Locale.ENGLISH, "%.2f", soma));
        });
    }
}